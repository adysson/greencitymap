namespace :markers do
  desc "set URL for previously saved markers"
  task set_url: :environment do
    Marker.all.each {|marker| marker.update!(url: "#{marker.id}-#{marker.title.parameterize}") }
  end
end
