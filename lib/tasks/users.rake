namespace :users do
  desc "gives the admin role to the first user"
  task set_admin_user: :environment do
    User.first.update!(role: "admin")
  end

  task set_user_for_existing_markers: :environment do
    Marker.update_all(user_id: User.find_by_role("admin").id)
  end
  
  task create_default_user_without_role: :environment do
    User.create(email: "adam.vasicek@seznam.cz", password: "1", password_confirmation: "1", name: "admin")
  end
end
