require 'test_helper'

class CustomPagesControllerTest < ActionController::TestCase
  setup do
    @page = custom_pages(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:pages)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create page" do
    assert_difference('Page.count') do
      post :create, page: { active: @page.active, body: @page.body, page_url: @page.page_url, position_in_menu: @page.position_in_menu, show_in_menu: @page.show_in_menu, title: @page.title }
    end

    assert_redirected_to page_path(assigns(:page))
  end

  test "should show page" do
    get :show, id: @page
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @page
    assert_response :success
  end

  test "should update page" do
    patch :update, id: @page, page: { active: @page.active, body: @page.body, page_url: @page.page_url, position_in_menu: @page.position_in_menu, show_in_menu: @page.show_in_menu, title: @page.title }
    assert_redirected_to page_path(assigns(:page))
  end

  test "should destroy page" do
    assert_difference('Page.count', -1) do
      delete :destroy, id: @page
    end

    assert_redirected_to pages_path
  end
end
