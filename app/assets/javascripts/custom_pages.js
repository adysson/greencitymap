// Place all the behaviors and hooks related to the matching controller here.
// All this logic will automatically be available in application.js.

function isUrlAvailable(inputField, url, current_page_url, textAvailable, textUnavailable) {
  var input = inputField,
      url = url + '/' + input.val() + '.json',
      responseClass = 'url_available',
      textAvailable = textAvailable,
      textUnavailable = textUnavailable,
      messageElement = input.parents('div.field').find('span.' + responseClass);

  messageElement.fadeOut(150).removeClass('available unavailable').text('');

  if (input.val() != current_page_url && input.val().length != 0) {
    $.getJSON(url, function(response) {
      if (messageElement.length == 0) {
        messageElement = $('<span>').addClass(responseClass).css('display', 'none');
      }

      if (response.urlAvailable) {
        messageElement.text(textAvailable).addClass('available');
      } else {
        messageElement.text(textUnavailable).addClass('unavailable');
      }
      input.after(messageElement.fadeIn(150));
    })
  }
}

