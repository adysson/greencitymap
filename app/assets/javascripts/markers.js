$(function() {
  showAndHideCommentsForm();

  if(contentBoxContainsOnlyFlashMessage()) {
    contentBoxAutoWidth();
    autoHideContentBox();
  }
  return false;
});

function showAndHideCommentsForm() {
  var commentsForm = $("form#new_comment");

  commentsForm.css("display", "none");

  $("div#comments h4").click(function(e) {
    if (commentsForm.css("display") == "block") {
      commentsForm.css("display", "none");
    }
    else {
      commentsForm.css("display", "block");
    }
  });
}
