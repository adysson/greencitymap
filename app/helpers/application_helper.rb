module ApplicationHelper
  def body_css_id
    body_css_id = controller.controller_name + "_"
    body_css_id += controller.action_name == "create" ? "new" : controller.action_name
    body_css_id
  end
end
