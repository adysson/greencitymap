class CustomPagesController < ApplicationController
  before_action :set_custom_page, only: [:show, :edit, :update, :destroy]
  before_action :set_custom_pages, only: [:index, :destroy]
  before_action :set_title, only: [:show, :edit]
  load_and_authorize_resource

  # GET /pages
  def index
    @title = "Seznam stránek - #{CustomPage::DEFAULT_TITLE}"

    respond_to do |format|
      format.html
      format.js
    end
  end

  # GET /pages/1
  def show
    respond_to do |format|
      format.html
      format.js
    end
  end

  # GET /pages/new
  def new
    @custom_page = CustomPage.new

    respond_to do |format|
      format.html
      format.js
    end
  end

  # GET /pages/1/edit
  def edit
    respond_to do |format|
      format.html
      format.js
    end
  end

  # POST /pages
  def create
    @custom_page = CustomPage.new(custom_page_params)

    respond_to do |format|
      if @custom_page.save
        format.html { redirect_to show_custom_page_url(@custom_page), notice: t("custom_pages.create.ok") }
        format.js { flash.now.notice = t("custom_pages.create.ok") }
      else
        format.html { render :new }
        format.js
      end
    end
  end

  # PATCH/PUT /pages/1
  def update
    respond_to do |format|
      if @custom_page.update(custom_page_params)
        format.html { redirect_to show_custom_page_url(@custom_page), notice: t("custom_pages.create.ok") }
        format.js { flash.now.notice = t("custom_pages.create.ok") }
      else
        format.html { render :edit }
        format.js
      end
    end
  end

  # DELETE /pages/1
  def destroy
    @custom_page.destroy
    respond_to do |format|
      format.html { redirect_to custom_pages_url, notice: t("custom_pages.destroy.ok") }
      format.js { flash.now.notice = t("custom_pages.destroy.ok") }
    end
  end

  def url_available
    respond_to do |format|
      format.json do
        render json: { urlAvailable: !CustomPage.exists?(page_url: params[:url_to_check]) }
      end
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_custom_page
      if current_user.present? && current_user.admin?
        @custom_page = CustomPage.find_by_page_url(params[:id])
      else
        @custom_page = CustomPage.where(active: true).find_by_page_url(params[:id])
      end
      @custom_page ||= not_found
    end

    def set_custom_pages
      @custom_pages = CustomPage.order(:page_url)
    end

    def set_title
      @title = @custom_page.full_title
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def custom_page_params
      params.require(:custom_page).permit(:title, :body, :active, :show_in_menu, :page_url, :position_in_menu)
    end
end
