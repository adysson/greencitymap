class CommentsController < ApplicationController
  load_and_authorize_resource only: :destroy

  def create
    @marker = Marker.find(params[:marker_id])
    @comment = @marker.comments.create(comment_params)

    if @comment.present?
      respond_to do |format|
        format.html { redirect_to marker_path(@marker), notice: "komentar ulozeny" }
        format.js
      end
    end
  end

  def destroy
    @comment = Comment.find(params[:id])
    @comment.destroy
    respond_to do |format|
      format.html { redirect_to marker_url(@comment.marker), notice: t("comments.destroy.ok") }
      format.js { flash.now.notice = t("comments.destroy.ok") }
    end
  end

  private

  def comment_params
    parameters = params.require(:comment).permit(:author_name, :body)
    parameters.merge(user_id: current_user.id) if current_user.present?
    parameters
  end
end
