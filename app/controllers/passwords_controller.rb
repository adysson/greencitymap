class PasswordsController < ApplicationController
  before_action :set_user_by_token, only: [:new, :create]

  def request_password
    respond_to do |format|
      format.html
      format.js
    end
  end

  def set_password_token
    @user = User.find_by_email(params[:email])

    respond_to do |format|
      if @user.present?
        token = Digest::SHA1.hexdigest(@user.email.to_s + Time.now.to_s)
        @user.update(renew_password_token: token)
        PasswordMailer.send_renew_password_token(token).deliver

        @permanentFlash = true
        format.html { redirect_to request_password_url, notice: t("passwords.request_password.check_your_email") }
        format.js { flash.now.notice = t("passwords.request_password.check_your_email") }
      else
        flash.now.notice = params[:email].strip.length == 0 ? t("messages.enter_email") : t("messages.bad_email")
        format.html { render "request_password" }
        format.js
      end
    end
  end

  def new
    redirect_to request_password_url, notice: t("passwords.new.invalid_token") if @user.nil?
  end

  def create
    @password_ok = nil
    respond_to do |format|
      if params[:user][:password].strip.length == 0
        flash.now.notice = t("messages.password_empty")
        format.html { render :new }
        format.js
      elsif params[:user][:password] != params[:user][:password_confirmation]
        flash.now.notice = t("messages.password_not_match")
        format.html { render :new }
        format.js
      elsif @user.nil?
        flash.now.notice = t("messages.bad_email")
        format.html { render :request_password }
        format.js
      elsif @user.update(password: params[:user][:password],
                         password_confirmation: params[:user][:password_confirmation])
        @password_ok = true
        @user.update(renew_password_token: nil)
        format.html { redirect_to login_url, notice: t("passwords.create.ok") }
        format.js { flash.now.notice = t("passwords.create.ok") }
      end
    end
  end

  private

  def set_user_by_token
    @user = User.find_by_renew_password_token(params[:token])
  end

  # refactor na volani super
  def set_title
    @title =  "Obnovení hesla - #{CustomPage::DEFAULT_TITLE}"
  end
end
