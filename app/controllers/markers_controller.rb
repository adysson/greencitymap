class MarkersController < ApplicationController
  before_action :authorize, only: [:edit, :update, :destroy]
  before_action :set_marker, only: [:show, :edit, :update, :destroy]
  before_action :set_marker_categories, only: [:new, :create, :edit, :update]

  load_and_authorize_resource

  # GET /markers
  def index
    @markers = Marker.all
  end

  # GET /markers/1
  def show
    @author_name = current_user.present? ? current_user.name : ""
    respond_to do |format|
      format.html { render "show", layout: false if params[:l] == "f" }
      format.js
    end
  end

  # GET /markers/new
  def new
    @marker = Marker.new
    build_marker_pictures
    render "new", layout: false if params[:l] == "f"
  end

  # GET /markers/1/edit
  def edit
    respond_to do |format|
      format.html { render "edit", layout: false if params[:l] == "f" }
      format.js
    end
  end

  # POST /markers
  def create
    @marker = Marker.new(marker_params)

    respond_to do |format|
      if @marker.save
        format.html { redirect_to @marker, notice: t("markers.create.ok") }
        format.js { flash.now.notice = t("markers.create.ok") }
      else
        format.html { render :new }
        format.js
      end
    end
  end

  # PATCH/PUT /markers/1
  def update
    respond_to do |format|
      if @marker.update(marker_params)
        format.html { redirect_to @marker, notice: t("markers.update.ok") }
        format.js { flash.now.notice = t("markers.update.ok")}
      else
        format.html { render :edit }
        format.js
      end
    end
  end

  # DELETE /markers/1
  def destroy
    @marker.destroy
    respond_to do |format|
      format.html { redirect_to root_url, notice: t("markers.destroy.ok") }

      # DO NOT USE
      # CURRENTLY NO WAY TO REMOVE DESTROYED MARKER FROM THE MAP ON FRONTEND
      format.js { flash.now.notice = t("markers.destroy.ok") }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_marker
      @marker = Marker.find(params[:id])
    end

    def set_marker_categories
      @marker_categories = [[t("markers.choose_type"), nil]]
      MarkerCategory.all.order(:title).collect do |category|
        @marker_categories << [category.title, category.id]
      end
    end

    def build_marker_pictures
      6.times { @marker.pictures.build }
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def marker_params
      parameters = params.require(:marker)
        .permit(:title, :content, :marker_category_id, :lat, :lng, :video_url, :video_iframe,
                pictures_attributes: [:id, :picture])

      if current_user.present?
        parameters.merge(user_id: current_user.id)
      else
        parameters
      end
    end
end
