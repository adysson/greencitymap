class UsersController < ApplicationController
  before_action :set_user, only: [:show, :edit, :update, :destroy]
  before_action :set_users, only: [:index, :update]
  before_action :set_roles, only: [:edit, :update]
  load_and_authorize_resource except: [:new, :create]

  def index
    respond_to do |format|
      format.html
      format.js
    end
  end

  def show
    respond_to do |format|
      format.html
      format.js
    end
  end

  def new
    @user = User.new

    respond_to do |format|
      format.html
      format.js
    end
  end

  def edit
    respond_to do |format|
      format.html
      format.js
    end
  end

  def create
    @user = User.new(user_params)

    respond_to do |format|
      if @user.save
        UserMailer.registration_email(@user).deliver
        format.html { redirect_to root_url, notice: t("users.create.ok") }
        format.js { flash.now.notice = t("users.create.ok") }
      else
        format.html { render :new }
        format.js
      end
    end
  end

  def update
    respond_to do |format|
      if @user.update(user_params)
        destination = current_user.admin? ? users_url : @user

        format.html { redirect_to destination, notice: t("users.update.ok") }
        format.js { flash.now.notice = t("users.update.ok") }
      else
        format.html { render :edit }
        format.js
      end
    end
  end

  def destroy
    if current_user.user? || (current_user.admin? && @user.id == current_user.id)
      session[:user_id] = nil
      destination = root_url
    elsif current_user.admin? && @user.id != current_user.id
      destination = users_url
    end
    @user.destroy

    respond_to do |format|
      format.html { redirect_to destination, notice: t("users.destroy.ok") }
      format.js { flash.now.notice = t("users.destroy.ok") }
    end
  end

  private

  def set_user
    @user = User.find(params[:id])
  end

  def set_users
    @users = User.order(:role, :email)
  end

  def set_roles
    @roles = []
    User::ROLES.each { |role| @roles << [t("users.roles.#{role}"), role] }
    @roles
  end

  def user_params
    parameters = params.require(:user).permit(:name, :email)
    parameters.merge!(role: params[:user][:role]) if current_user.present? && current_user.admin?

    if current_user.blank?
      parameters.merge!(
        password: params[:user][:password],
        password_confirmation: params[:user][:password_confirmation]
      )
    end
    parameters
  end

  # refactor na volani super
  def set_title
    titles = {
      new: "Registrace uživatele",
      edit: "Úprava údajů",
      index: "Seznam uživatelů",
      show: "Detail účtu"
    }
    @title = "#{titles[action_name.to_sym]} - #{CustomPage::DEFAULT_TITLE}"
  end
end
