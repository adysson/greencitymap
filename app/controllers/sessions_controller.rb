class SessionsController < ApplicationController
  def new
    redirect_to root_url, notice: t("sessions.new.already_logged_in") if current_user.present?
    @user = User.new

    respond_to do |format|
      format.html
      format.js
    end
  end

  def create
    @user = User.find_by_email(params[:email])

    respond_to do |format|
      if @user && @user.authenticate(params[:password])
        session[:user_id] = @user.id
        @current_user = current_user
        format.html { redirect_to root_url, notice: t("sessions.create.ok") }
        format.js { flash.now.notice = t("sessions.create.ok") }
      else
        flash.now.alert = t("sessions.create.ko")
        format.html { render :new }
        format.js
      end
    end
  end

  def destroy
    session[:user_id] = nil

    respond_to do |format|
      format.html { redirect_to root_url, notice: t("sessions.destroy.logged_out") }
      format.js { flash.now.notice = t("sessions.destroy.logged_out") }
    end
  end

  # refactor na volani super
  def set_title
    @title = "Přihlášení - #{CustomPage::DEFAULT_TITLE}"
  end
end
