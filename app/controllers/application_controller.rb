class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception
  before_action :current_user
  before_action :set_markers
  before_action :set_title

  rescue_from CanCan::AccessDenied do |exception|
    redirect_to root_url, alert: t("messages.access_denied")
  end

  rescue_from ActionController::RoutingError do |exception|
    error_404
  end

  private

  # toto bude potreba v budoucnu omezit podle zobrazene mapy
  # a zbytek nacitat AJAXem
  def set_markers
    @markers = Marker.all
  end

  def set_title
    @title = CustomPage::DEFAULT_TITLE
  end

  def current_user
    @current_user ||= User.find(session[:user_id]) if session[:user_id].present?
  end

  def authorize
    redirect_to root_url if current_user.blank?
  end

  def not_found
    raise ActionController::RoutingError.new("not found")
  end

  def error_404
    respond_to do |format|
      format.html { render "error_404", status: :not_found }
      format.js { render "error_404", status: :not_found  }
    end
  end
end
