class PasswordMailer < ActionMailer::Base
  default from: "info@zasadsvemesto.cz"

  def send_renew_password_token(token)
    @user = User.find_by_renew_password_token(token)
    @url = password_token_url(token)
    mail(to: @user.email, subject: t("mailer.password.renew_password_token.subject"))
  end
end
