class UserMailer < ActionMailer::Base
  default from: "info@zasadsvemesto.cz"

  def registration_email(user)
    @user = user
    mail(to: @user.email, subject: t("mailer.user.registration.subject"))
  end
end
