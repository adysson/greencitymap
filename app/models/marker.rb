class Marker < ActiveRecord::Base
  include ActionView::Helpers::TextHelper
  include ActionView::Helpers::SanitizeHelper

  VIDEO_DIMENSIONS = {
    content: { width: 360, height: 216 }
  }

  belongs_to :user
  has_one :marker_category
  has_many :comments
  has_many :pictures
  accepts_nested_attributes_for :pictures

  validates_presence_of :title, :lat, :lng
  validates_uniqueness_of :url, on: :update

  before_create do
    self.content = strip_tags(simple_format(self.content))
  end
  after_create do
    self.url = self.to_param
    self.save
  end
  before_create :parse_video_url_to_iframe
  before_update :parse_video_url_to_iframe

  def full_title
    "#{title} - #{CustomPage::DEFAULT_TITLE}"
  end

  def gps
    "#{lat.round(7)}N, #{lng.round(7)}E"
  end

  def parse_video_url_to_iframe
    if self.video_url =~ /youtube\.com\/watch(.+)/
      position = 8 + (self.video_url =~ /watch/)
      youtube_id = self.video_url[position..-1]
      self.video_iframe = "<iframe width='VIDEO_WIDTH' height='VIDEO_HEIGHT' src='https://www.youtube.com/embed/" + youtube_id + "' frameborder='0' allowfullscreen></iframe>"
    elsif self.video_url =~ /vimeo\.com\/(.+)/
      vimeo_id = self.video_url.scan(/\d/).join
      self.video_iframe = '<iframe src="https://player.vimeo.com/video/' + vimeo_id + '" width="VIDEO_WIDTH" height="VIDEO_HEIGHT" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>'
    elsif self.video_url =~ /(<iframe(.+)>(.*)<\/iframe>)|(<embed(.+)>(.*)<\/embed>)|(<video(.+)>(.+)<\/video>)/
      self.video_iframe = self.video_url
      self.video_url = nil
    end

    if self.video_url.present? && self.video_url !~ /^http/
      self.video_url = "http://#{video_url}"
    end
  end

  def to_param
    "#{id}-#{title.parameterize}"
  end

  def video_iframe_formatted(format)
    raise ArgumentError unless VIDEO_DIMENSIONS.has_key? format

    video_iframe.gsub("VIDEO_WIDTH", VIDEO_DIMENSIONS[format][:width].to_s)
      .gsub("VIDEO_HEIGHT", VIDEO_DIMENSIONS[format][:height].to_s)
  end
end
