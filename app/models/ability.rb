class Ability
  include CanCan::Ability

  def initialize(user)
    user ||= User.new
    alias_action :index, to: :list
    alias_action :show, to: :see

    if user.admin?
      can :manage, :all
    elsif user.user?
      can :manage, Marker, user_id: user.id
      can :read, Marker
      cannot :manage, User
      can :manage, User, id: user.id
      cannot :list, User
      cannot :manage, CustomPage
      can :see, CustomPage
      cannot :destroy, Comment
    end
  end
end
