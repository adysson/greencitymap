class Picture < ActiveRecord::Base
  belongs_to :marker

  has_attached_file :picture,
    styles: { medium: '960x960>', thumb: '175x175>' },
    storage: :dropbox,
    dropbox_credentials: Rails.root.join("config/dropbox.yml"),
    dropbox_visibility: 'public',
    path: "/websites/greencitymap/:id_:basename_:style.:extension",
    url: "/websites/greencitymap/:id_:basename_:style.:extension"

  validates_attachment_content_type :picture, content_type: /\Aimage\/.*\Z/

  def url_thumb
    picture.url(:thumb)
  end

  def url_medium
    picture.url(:medium)
  end
end
