class MarkerCategory < ActiveRecord::Base
  belongs_to :marker

  validates :title, presence: true, uniqueness: true
end
