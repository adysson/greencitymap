class CustomPage < ActiveRecord::Base
  validates_presence_of :title, :page_url, :body
  validates_uniqueness_of :page_url

  # presunout do cs.yml
  DEFAULT_TITLE = "Zasaď své město"

  def inactive?
    !active?
  end

  def self.pages_for_menu
    where('show_in_menu = ?', true).select('title, page_url, body').order('position_in_menu')
  end

  def title
    super.gsub(" ", "\u00A0")
  end

  def full_title
    "#{title} - #{DEFAULT_TITLE}"
  end

  def to_param
    page_url
  end

  def url_to_url_available
    new_record? ? "/custom_pages/url_available" : "/custom_pages/#{to_param}/url_available"
  end
end
