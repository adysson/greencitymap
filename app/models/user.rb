class User < ActiveRecord::Base
  ROLES = %i[user admin]

  has_secure_password

  has_many :markers
  has_many :markers

  validates_presence_of :email
  validates_uniqueness_of :email
  validates_presence_of :password, on: :create

  def admin?
    role == "admin"
  end

  def guest?
    !admin? && !user?
  end

  def user?
    role == "user"
  end

  def title_for_user_menu
    name.present? ? "#{name} (#{email})" : email
  end
end
