# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)
MarkerCategory.create(title: "místo ve městě")
Rake::Task["users:create_default_user_without_role"].invoke
Rake::Task["users:set_admin_user"].invoke
