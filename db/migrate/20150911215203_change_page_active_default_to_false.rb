class ChangePageActiveDefaultToFalse < ActiveRecord::Migration
  def up
    change_column :pages, :active, :boolean, default: false
  end

  def down
    change_column :pages, :active, :boolean, default: nil
  end
end
