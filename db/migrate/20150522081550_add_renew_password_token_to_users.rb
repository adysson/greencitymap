class AddRenewPasswordTokenToUsers < ActiveRecord::Migration
  def change
    add_column :users, :renew_password_token, :string, default: nil
  end
end
