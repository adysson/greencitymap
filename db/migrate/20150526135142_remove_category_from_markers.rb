class RemoveCategoryFromMarkers < ActiveRecord::Migration
  def change
    remove_column :markers, :category_id, :integer, default: nil
  end
end
