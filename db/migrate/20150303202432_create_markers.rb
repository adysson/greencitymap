class CreateMarkers < ActiveRecord::Migration
  def change
    create_table :markers do |t|
      t.string :title
      t.string :content
      t.integer :category_id

      t.timestamps
    end
  end
end
