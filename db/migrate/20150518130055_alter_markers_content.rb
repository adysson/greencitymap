class AlterMarkersContent < ActiveRecord::Migration
  def up
    change_column :markers, :content, :text
  end

  def down
    change_column :markers, :content, :string
  end
end
