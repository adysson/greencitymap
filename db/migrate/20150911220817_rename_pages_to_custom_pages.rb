class RenamePagesToCustomPages < ActiveRecord::Migration
  def change
    rename_table :pages, :custom_pages
  end
end
