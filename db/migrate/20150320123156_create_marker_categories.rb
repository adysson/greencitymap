class CreateMarkerCategories < ActiveRecord::Migration
  def change
    create_table :marker_categories do |t|
      t.string :title

      t.timestamps
    end
    add_reference :markers, :marker_category
    MarkerCategory.create(title: "místo ve městě")
  end
end
