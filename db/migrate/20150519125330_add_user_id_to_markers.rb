class AddUserIdToMarkers < ActiveRecord::Migration
  def change
    add_column :markers, :user_id, :integer, default: nil
  end
end
