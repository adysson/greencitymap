class AddUrlToMarkers < ActiveRecord::Migration
  def change
    add_column :markers, :url, :string, default: nil
  end
end
