class CreateComments < ActiveRecord::Migration
  def change
    create_table :comments do |t|
      t.string :author_name
      t.text :body
      t.references :marker, index: true
      t.references :user, index: true

      t.timestamps
    end
  end
end
