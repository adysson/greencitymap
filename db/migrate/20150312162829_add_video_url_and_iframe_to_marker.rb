class AddVideoUrlAndIframeToMarker < ActiveRecord::Migration
  def change
    add_column :markers, :video_url, :string
    add_column :markers, :video_iframe, :string
  end
end
